<?php
add_action( 'wp_enqueue_scripts', 'shapely_scripts' );
add_action( 'wp_enqueue_scripts', 'mda_enqueue_styles' );
function mda_enqueue_styles() {
 
    $parent_style = 'shapely-style';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'mda-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}